package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	progPath string
	progName = "code"
)

func init() {
	var err error
	progPath, err = getProgramPath()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: failed to obtain executable path (%v)\n", progName, err)
		os.Exit(1)
	}
	progName = filepath.Base(progPath)
}

func main() {
	var err error
	initConfig()
	err = launch()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: failed to launch (%v)\n", progName, err)
		os.Exit(1)
	}
	os.Exit(0)
}

func launch() error {
	viper.SetDefault("path", filepath.ToSlash(filepath.Clean(filepath.Join(filepath.Dir(progPath), "VSCode"))))
	baseDir := viper.GetString("path")
	os.Unsetenv("VSCODE_DEV")
	os.Setenv("ELECTRON_RUN_AS_NODE", "1")
	binPath := filepath.Join(baseDir, "Code - Insiders.exe")
	args := []string{
		filepath.Clean(filepath.Join(baseDir, "resources/app/out/cli.js")),
	}
	args = append(args, os.Args[1:]...)
	cmd := exec.Command(binPath, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func initConfig() {
	var err error
	viper.AddConfigPath(filepath.Dir(progPath))
	home, err := homedir.Dir()
	if err == nil {
		viper.AddConfigPath(home)
	}
	name := progName
	if strings.HasSuffix(name, ".exe") {
		name = name[:len(name)-4]
	}
	viper.SetConfigName(name)
	_ = viper.ReadInConfig()
}

func getProgramPath() (string, error) {
	p, err := os.Executable()
	if err != nil {
		return "", err
	}
	return p, nil
}
